return {
  {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"},
  {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"},
  {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"},
  {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"},

  -- string:
  {"upper", "lower"},

  -- math:
  {"ceil", "floor"},
  {"min", "max"},

  -- io:
  {"stderr", "stdout"},

  -- bitwise:
  {"<<", ">>"},

  -- relational:
  {"<=", ">"},
  {">=", "<"},
  {"==", "~="},
  {"true", "false"},
}
